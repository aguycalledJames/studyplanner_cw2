package Controller;

import Model.Assignment;
import Model.Module;
import Model.Profile;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javafx.geometry.Pos.CENTER;

public class MainController {
    public static UIManager ui = new UIManager();

    private Profile profile;

    public void loadDashboard(GridPane moduleGridPane, Profile currentProfile) {
        profile = currentProfile;
        List<Button> temp = new ArrayList<Button>();
        int i = 0;
        for (Module module : profile.getModules()) {
            Button btn = new Button();
            btn.setText(module.getName());
            btn.setPrefHeight(80);
            btn.setPrefWidth(130);
            btn.setWrapText(true);
            btn.setAlignment(CENTER);
            btn.textAlignmentProperty().set(TextAlignment.CENTER);
            GridPane.setHalignment(btn, javafx.geometry.HPos.CENTER);
            GridPane.setValignment(btn, javafx.geometry.VPos.CENTER);
            GridPane.setMargin(btn, new Insets(40, 0, 10, 0));
            btn.setOnAction(e ->  {
                try {
                    Stage window = (Stage)((Node)e.getSource()).getScene().getWindow();
                    ui.loadModule(currentProfile, module);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            });
            temp.add(btn);
            i++;
            //moduleGridPane.getChildren().addAll(btn);
        }
        int count = 0;
        for(Button btn : temp){
            switch (count){
                case 0:
                    moduleGridPane.add(btn, 0, 0, 1, 1);
                    count++;
                    break;
                case 1:
                    moduleGridPane.add(btn, 0, 1, 1, 1);
                    count++;
                    break;
                case 2:
                    moduleGridPane.add(btn, 0, 2, 1, 1);
                    count++;
                    break;
                case 3:
                    moduleGridPane.add(btn, 1, 0, 1, 1);
                    count++;
                    break;
                case 4:
                    moduleGridPane.add(btn, 1, 1, 1, 1);
                    count++;
                    break;
                case 5:
                    moduleGridPane.add(btn, 1, 2, 1, 1);
                    count++;
                    break;
                default:
                    System.out.println("ERROR: Too many modules add");
                    break;
            }
        }

    }

    public MainController(){
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

}
