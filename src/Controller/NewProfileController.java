package Controller;

import Model.Profile;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

import static Model.Profile.isValid;

public class NewProfileController {
    // This class will mainly be used for data validation in creating a new account
    Stage stage;
    ProfileController pc;
    @FXML private Button createProfileButton;   //button to create a profile
    @FXML private Button browseFileButton;      //button to browse to a data file
    @FXML private TextField profileNameField;   //field to input profile's name
    @FXML private TextField dataFilePathField;  //field to input path to hub file
    @FXML private PasswordField passwordField;      //field for user to input password
    boolean inputVerified = true;

    public void initialize() {
        createProfileButton.setOnAction(value -> {
            try {
                createProfileButtonClick();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        browseFileButton.setOnAction(value -> {
            try {
                browseButtonClick();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    void initData(ProfileController controller) {
        this.pc = controller;
    }

    private void createProfileButtonClick() throws Exception {
        if (profileNameField.getText().trim().equals("") ||
                dataFilePathField.getText().trim().equals("") ||
                passwordField.getText().trim().equals("")) {
            inputVerified = false;
        }
        if (inputVerified) {
            Profile profile = new Profile();
            File hubFile = new File(dataFilePathField.getText());
            profile.setName(profileNameField.getText());

            Hasher hashPword = new Hasher(){};
            hashPword.setHashedValue(passwordField.getText());
            profile.setPassword(hashPword.getHashedValue());

            Profile.InitialiseStudyProfile(profile, hubFile);
            pc.setProfile(profile);
            pc.saveProfile(profile);
            pc.addProfileToListView(profile);
            stage = (Stage) createProfileButton.getScene().getWindow();
            stage.hide();
        }
    }

    private void browseButtonClick() {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Hub File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("XML", "*.xml")
        );
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            if (isValid(file)) {
                dataFilePathField.setText(file.getPath());
            } else {
                System.out.println("Invalid file selected");
            }
        }
    }
}
